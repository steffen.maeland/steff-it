
FROM tensorflow/tensorflow:latest-gpu 

WORKDIR /app

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update && apt-get install -y python3-tk
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
