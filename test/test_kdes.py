import sys, os
import pytest
import numpy as np
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

sys.path.insert(0, os.path.abspath('..'))
from nn_kde import NnKde




@pytest.fixture
def test_data():
    ''' Return data to test on '''
    np.random.seed(0)
    X1 = np.random.normal(loc=0.2, scale=0.1, size=3000)
    X2 = np.random.normal(loc=0.6, scale=0.3, size=4500)
    X = np.concatenate((X1, X2))
    X = X[(X > 0.0) & (X < 1.0)]

    return X

@pytest.fixture
def nn_test_data():
    ''' Classifier-type test data '''

    X1 = np.load('testdata/template_class0_xg_no_qcd.npy')
    X2 = np.load('testdata/template_class1_xg_no_qcd.npy')

    return (X1, X2)


def test_init():
    ''' Test simple initialisation '''
    kde = NnKde()
    assert True


def make_plot(data, kde, figname):
    ''' Plot results '''

    fig = plt.figure()

    xvals = np.linspace(0, 1, 100)
    prob = kde.evaluate(xvals)

    plt.hist(data, bins=30, range=(0,1), density=True, label='Data')
    plt.plot(xvals, prob, label='Method: {}, bw : ({:.3})'.format(
        kde._bandwidth_selector, kde._bandwidth))
    plt.legend()

    print('Saving figure as {}'.format(figname))
    plt.savefig(figname)


#@pytest.mark.skip
def test_fixed_bandwidth(test_data):
    ''' Test KDE with fixed bandwidth of 0.05 '''

    kde = NnKde(bandwidth=0.05)
    kde.fit(test_data)

    xvals = np.linspace(0, 1, 10)
    prob = kde.evaluate(xvals)
    assert not np.isnan(prob).any()

    make_plot(test_data, kde, 'test_fixed_bandwidth.png')


@pytest.mark.skip
def test_silverman(test_data):
    ''' Test KDE with Silverman bandwidth selection '''

    kde = NnKde(bandwidth='silverman')
    kde.fit(test_data)

    xvals = np.linspace(0, 1, 10)
    prob = kde.evaluate(xvals)
    assert not np.isnan(prob).any()

    make_plot(test_data, kde, 'test_silverman_bandwidth.png')


@pytest.mark.skip
def test_crossvalidation(test_data):
    ''' Test KDE with cross-validation bandwidth selection '''

    kde = NnKde(bandwidth='cross-validation', verbose=True)
    kde.fit(test_data, method='random', spacing='log', n_points=10)

    xvals = np.linspace(0, 1, 10)
    prob = kde.evaluate(xvals)
    assert not np.isnan(prob).any()

    make_plot(test_data, kde, 'test_crossvalidation_bandwidth.png')



@pytest.mark.skip
def test_binned_data(test_data):
    ''' Test KDE with binned data '''

    #kde = NnKde(bandwidth='cross-validation', binned=True, verbose=True)
    kde = NnKde(binned=True, verbose=True)
    kde.fit(test_data)
    
    xvals = np.linspace(0, 1, 200)
    prob = kde.evaluate(xvals)
    assert not np.isnan(prob).any()

    make_plot(test_data, kde, 'test_binned_data.png')


#@pytest.mark.skip
def test_classifier_data(nn_test_data):
    ''' Test xgboost classifier data '''
    
    x0 = nn_test_data[0]
    kde0 = NnKde(bandwidth='cross-validation', binned=True, verbose=True)
    #kde0 = NnKde(bandwidth='cross-validation', verbose=True)
    kde0.fit(x0, spacing='log')

    x1 = nn_test_data[1]
    kde1 = NnKde(bandwidth='cross-validation', binned=True)
    #kde1 = NnKde(bandwidth='cross-validation', binned=False)
    kde1.fit(x1, method='random')

    xvals = np.linspace(0, 1, 100)
    prob0 = kde0.evaluate(xvals)
    prob1 = kde1.evaluate(xvals)

    fig = plt.figure()

    plt.hist(x0, bins=30, range=(0,1), density=True, label='Data 0', alpha=0.5)
    plt.hist(x1, bins=30, range=(0,1), density=True, label='Data 1', alpha=0.5)
    plt.plot(xvals, prob0, label='Method: {}, bw : ({:.3})'.format(
        kde0._bandwidth_selector, kde0._bandwidth))
    plt.plot(xvals, prob1, label='Method: {}, bw : ({:.3})'.format(
        kde1._bandwidth_selector, kde1._bandwidth))
    plt.legend()

    figname = 'test_classifier.png'
    print('Saving figure as {}'.format(figname))
    plt.savefig(figname)